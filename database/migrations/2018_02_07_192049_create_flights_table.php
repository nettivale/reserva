<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flights', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name',100)->nullable();
            $table->string('last_name',100)->nullable();
            $table->integer('traveler_count')->nullable();
            $table->string('email',100)->nullable();
            $table->string('phone',20)->nullable();
            $table->dateTime('travel_from_date')->nullable();
            $table->dateTime('travel_to_date')->nullable();
            $table->string('travel_from_loc',20)->nullable();
            $table->string('travel_to_loc',20)->nullable();
            $table->longText('other_flight_details')->nullable();
            $table->dateTime('visa_interview_date')->nullable();
            $table->string('visa_consulate',100)->nullable();
            $table->string('referral_src_code',20)->nullable();
            $table->tinyInteger('is_traveler')->nullable();
            $table->decimal('flight_price',9)->nullable();
            $table->decimal('total_payment',9)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flights');
    }
}
