<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateReferralSrcsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referral_src', function (Blueprint $table) {
            $table->increments('id');
            $table->string('referral_src_code',30)->nullable();
            $table->string('description',255)->nullable();
            $table->timestamps();
        });

        $data = array(
            array( 'referral_src_code' => 'SearchEngine', 'description' => 'Search engine (Google)' ),
            array( 'referral_src_code' => 'Blog', 'description' => 'Blog article' ),
            array( 'referral_src_code' => 'DiscBoard', 'description' => 'Discussion board' ),
            array( 'referral_src_code' => 'SocMedia', 'description' => 'Social media (Facebook etc.)' ),
            array( 'referral_src_code' => 'Quora', 'description' => 'Quora' ),
            array( 'referral_src_code' => 'FamFriends', 'description' => 'Family and friends' ),
            array( 'referral_src_code' => 'NotSure', 'description' => "I'm not sure" )
        );
        DB::table('referral_src')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referral_src');
    }
}
