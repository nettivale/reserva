@extends('layouts.app')

@section('content')
<section id="fh5co-home" data-section="home" class="formHeader"  data-stellar-background-ratio="0.5">
    <div class="gradient"></div>
    <div class="container">
        <div class="text-wrap headerText">
            <div class="text-inner valign-bottom">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 text-center">
                        <h1 class="to-animate">Fill details - Pay - get Itinerary via Email</h1>
                        <h2 class="to-animate">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec varius turpis ut varius dignissim. Suspendisse quis venenatis risus, sit amet auctor ex. Morbi congue finibus nibh sit amet dapibus. Sed dignissim enim venenatis nisl porta dictum.</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="fh5co-services" data-section="services">
    <div class="fh5co-services">
        <div class="container">
            <div class="row">
                <div class="col-md-12 section-heading mysection-heading text-center">
                    <h2 class="to-animate"><span>Flight reservation form(24 hours delivery)</span></h2>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 subtext">
                            <h3 class="to-animate">Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                    Suspendisse nec mi pellentesque,condimentum leo id, gravida felis.  </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<form class="formContainer">    
    <div class="container">
        <form action="{{ route('flight.store') }}" method="POST">
        {{ csrf_field() }}
            <div class="row divTravellerContainer">
                <h4>Traveler Details</h4>
                <div class="form-group col-md-6">
                    <small>Email *</small>
                    <input type="email" name="email" class="form-control" required />
                </div>
                <div class="form-group col-md-6">
                    <small>Phone *</small>
                    <input type="tel" name="phone" class="form-control" required />
                </div>
                <h4>Guest Info</h4>
                <div class="col-md-12 divTraveller trav-template">
                    <div class="form-group col-md-6">
                        <input type="text" class="form-control fname" placeholder="Enter first name" />
                    </div>
                    <div class="form-group col-md-5">
                        <input type="text" class="form-control lname" placeholder="Enter last name" />
                    </div>
                    <div class="form-group col-md-1 btnDiv">
                        <a href="" class="btn btn-primary btnTraveller"><i class="fa fa-plus"></i> Add</a>
                    </div>
                </div>
            </div>
            <div class="row divFlightContainer">
                <h4>Flight Details</h4>
                <div class="form-group col-md-6">
                    <small>Departing on *</small>
                    <input type="date" name="travel_from_date" class="form-control" required />
                </div>
                <div class="form-group col-md-6">
                    <small>From/To (City/Airport) *</small>
                    <input type="text" id="txtFromLoc" name="travel_from_loc" class="form-control" required/>
                </div>
                <div class="form-group col-md-6">
                    <small>Returning on</small>
                    <input type="date" name="travel_to_date" class="form-control" />
                </div>
                <div class="form-group col-md-6">
                    <small>From/To (City/Airport)</small>
                    <input type="text" name="travel_to_loc" class="form-control" />
                </div>
                <div class="form-group col-md-12">
                    <small>Other details</small>
                    <textarea class="form-control" name="other_flight_details"></textarea>
                </div>
                <h4>General Information</h4>
                <div class="form-group col-md-4">
                    <small>Visa interview date</small>
                    <input type="date" name="visa_interview_date" class="form-control" />
                </div>
                <div class="form-group col-md-4">
                    <small>Consulate</small>
                    <input type="text" name="visa_consulate" class="form-control" />
                </div>
                <div class="form-group col-md-4">
                    <small>How did you hear about us?</small>
                    <select name="referral_src_code" class="form-control">
                        @foreach($ref as $r)
                            <option value="{{ $r->referral_src_code }}">{{ $r->description }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-12">
                    <label>Are you one of the travellers making the payment? *</label>
                    <input type="radio" name="is_traveller" value="1" /> Yes 
                    <input type="radio" name="is_traveller" value="0" /> No
                </div>
            </div>
            <div class="row divOrderContainer">
                <h4>Order Summary</h4>
                <div class="form-group col-md-12">
                    <small>Flight Itinerary</small> 
                    <input type="text" id="txtFlightPrice" name="flight_price" value="0" readonly />
                </div>
                <div class="form-group col-md-12">
                    <small>Total payment</small> 
                    <input type="text" id="txtTotalPayment" name="total_payment" value="0" readonly />
                </div>
            </div>
            <button class="primary">PAY NOW</button>
        </form>
    </div>
</form>

@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        var initHandlers = function(){
            $('.btnTraveller').on('click',function(e){
                e.preventDefault();
                var obj = $(this);
                
                if(obj.hasClass('btn-primary')){
                    var fname = obj.closest('.divTraveller').find('.fname');
                    var lname = obj.closest('.divTraveller').find('.lname');
                    if(fname.val() == "") fname.addClass('error');
                    if(lname.val() == "") lname.addClass('error');
                    
                    if(fname.val() != "" && lname.val() != ""){
                        var cnt = $('.traveller').length + 1;
                        var trav_temp = $('.trav-template').clone().removeClass('trav-template').addClass('traveller');
                        trav_temp.find('.btnDiv a').removeClass('btn-primary').addClass('btn-danger').click(function(){
                            $(this).closest('.divTraveller').remove();
                            calculatePrice();
                        });
                        trav_temp.find('.fname').attr('name','fname[' + cnt + ']').attr('required','required');
                        trav_temp.find('.lname').attr('name','lname[' + cnt + ']').attr('required','required');
                        trav_temp.find('.btnDiv a').html('<i class="fa fa-minus"></i> Remove');
                        trav_temp.appendTo('.divTravellerContainer');
                        fname.val('');
                        lname.val('');
                    }
                }
            });

            $(".btnTraveller").on("click", function(){
                calculatePrice();
            });

            $(".fname,.lname").on('keyup', function(){
                var obj = $(this);
                if(obj.val() == '') obj.addClass('error');
                else obj.removeClass('error');
            });

            $( "#txtFromLoc" ).autocomplete({
                source: function( request, response ) {
                    $.ajax({
                        url: "https://api.sandbox.amadeus.com/v1.2/airports/autocomplete",
                        dataType: "json",
                        type: 'GET',
                        data: {
                            apikey: "sd4yRuYt7y2Yl2mWi0vcG85nksj2pcvB",
                            term: request.term
                        },
                        success: function( data ) {
                            response( data );
                        }
                    });
                },
                minLength: 3,
                select: function( event, ui ) {
                    log( ui.item ?
                        "Selected: " + ui.item.label :
                        "Nothing selected, input was " + this.value);
                },
                open: function() {
                    $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
                },
                close: function() {
                    $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
                }
            });
        };

        var calculatePrice = function(){
            var cnt = $('.traveller').length;
            var price = {!! $agency->price_6h !!} * cnt;
            $("#txtFlightPrice,#txtTotalPayment").val(price);
        };

        var build = function(){
            initHandlers();
        };
        build();
    });
</script>
@endpush