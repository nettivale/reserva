<header role="banner" id="fh5co-header">
    <div class="fluid-container">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <!-- Mobile Toggle Menu Button -->
                <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i></i></a>
                <a  href="{{ route('home') }}"><img class="logo" src="{{ URL::to('/') }}/img/logo.png" alt="reserva"></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right navbottom">
                    <li class="active"><a href="{{ route('home') }}" data-nav-section="home"><span>HOME</span></a></li>
                    <li><a href="#" data-nav-section="pricing"><span>PRICING</span></a></li>
                    <li><a href="#" data-nav-section="services"><span>SERVICES</span></a></li>
                    <li><a href="#" data-nav-section="faq"><span>FAQ</span></a></li>
                    <li class="call-to-action"><a class="log-in" href="#"><span>CONTACT US</span></a></li>
                </ul>
            </div>
        </nav>
    </div>
</header>