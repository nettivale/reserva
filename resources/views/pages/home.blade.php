@extends('layouts.app')

@section('content')
	<section class="homeImage" id="fh5co-home" data-section="home" style="background-image: url(img/homeImage1.jpg); background-repeat: no-repeat;"  data-stellar-background-ratio="0.5">
		<div class="gradient"></div>
			<div class="container">
				<div class="text-wrap headerText">
					<div class="text-inner valign-bottom">
						<div class="row">
							<div class="col-md-8 col-md-offset-2 bottom-align-text alignLeft">
								<h1 class="to-animate text-left">Applying</h1>
								<h1 class="to-animate text-left">for visa traveling?</h1>
								<h2 class="to-animate text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec varius turpis ut varius dignissim. Suspendisse quis venenatis risus, sit amet auctor ex. Morbi congue finibus nibh sit amet dapibus. Sed dignissim enim venenatis nisl porta dictum.</h2>
								<div class="call-to-action">
									<a href="#" class="learnMore to-animate"><i class="icon-learnMore"></i> Learn More</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
	</section>
	<section id="fh5co-services" data-section="services">
		<div class="fh5co-services">
			<div class="container">
				<div class="row">
					<div class="col-md-12 section-heading text-center">
						<h2 class="to-animate"><span>Three easy steps</span></h2>
						<div class="row">
							<div class="col-md-8 col-md-offset-2 subtext">
								<h3 class="to-animate">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sit amet convallis mi, eleifend sodales dolor. Curabitur sed magna quis massa pellentesque pulvinar vel vel nibh. Fusce in libero id dui commodo bibendum. </h3>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4 text-center">
						<div class="box-services">
							<div class="icon to-animate">
								<img class="logo" src="img/step1.png" alt="step1">
							</div>
							<div class="fh5co-post to-animate">
								<h3>Select Plan</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sit amet convallis mi, eleifend sodales dolor. Curabitur sed magna quis massa pellentesque pulvinar vel vel nibh.</p>
							</div>
						</div>
					</div>
					<div class="col-md-4 text-center">
						<div class="box-services">
							<div class="icon to-animate">
								<img class="logo" src="img/step2.png" alt="step1">
							</div>
							<div class="fh5co-post to-animate">
								<h3>Submit Travel Details and Pay</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sit amet convallis mi, eleifend sodales dolor. Curabitur sed magna quis massa pellentesque pulvinar vel vel nibh.</p>
							</div>
						</div>
					</div>
					<div class="col-md-4 text-center">
						<div class="box-services">
							<div class="icon to-animate">
								<img class="logo" src="img/step3.png" alt="step1">
							</div>
							<div class="fh5co-post to-animate">
								<h3>Receive Itinerary in Email</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sit amet convallis mi, eleifend sodales dolor. Curabitur sed magna quis massa pellentesque pulvinar vel vel nibh.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-offset-3 col-md-2 hidden-sm hidden-xs text-center">
						<div class="box-services">
							<div class="icon to-animate">
								<img class="logo" src="img/arrow1.png" alt="step1">
							</div>
						</div>
					</div>
					<div class="col-md-offset-2 col-md-2 hidden-sm hidden-xs text-center">
						<div class="box-services">
							<div class="icon to-animate">
								<img class="logo" src="img/arrow1.png" alt="step1">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="fh5co-pricing" data-section="pricing" style="background-image: url(img/pricingImg.jpg); background-repeat: round;" >
		<div class="fh5co-pricing">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12 section-heading text-right headerPadding">
						<h2 class="to-animate"><span>Flight reservation pricing</span></h2>
					</div>
				</div>	
				<div class="row">
					<div class="clearfix visible-sm-block"></div> 
					<div class="col-md-offset-6 col-md-3 col-sm-6 to-animate">
						<div class="price-box to-animate">
							<h2 class="pricing-plan">Regular</h2>
							<div class="price"><sup class="currency">$</sup>15<small>per traveler</small></div>
							<p>24 hour email delivery</p>
							<hr>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sit amet convallis mi, eleifend sodales dolor.</p>
							<p><a href="{{ route('flight.create') }}" class="btn btn-primary">ORDER NOW</a></p>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="price-box to-animate">
							<h2 class="pricing-plan">Enterprise</h2>
							<div class="price"><sup class="currency">$</sup>25<small>per traveler</small></div>
							<p>6 hour email delivery</p>
							<hr>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sit amet convallis mi, eleifend sodales dolor.</p>
							<p><a href="{{ route('flight.create') }}" class="btn btn-primary">ORDER NOW</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="container-fluid contactContainer">
		<div class="col-md-offset-2 col-md-8">
			<form class="formContactUS">
				<div class="form-group row">
					<div class="col-lg-6">
						<input type="text" class="form-control fname" placeholder="Enter email" />
					</div>
					<div class="col-lg-6">
						<input type="text" class="form-control lname" placeholder="Enter fullname" />
					</div>
				</div>
				<div class="form-group row">
					<div class="col-lg-12">
						<textarea name="message" id="message" class="form-control" rows="4" placeholder="Message" required></textarea>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-lg-4">
					</div>
					<div class="col-lg-8"> 
						<button class="btn btn-primary" id="btnSubmit">Submit</button>
					</div>
				</div>
			</form>
		</div>  
	</div>
@endsection

@push('scripts')

@endpush
