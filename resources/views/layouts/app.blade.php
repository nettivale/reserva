<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" />
    <!-- Style -->
    <link rel="stylesheet" href="{{ asset('css/helper/style.css')}}">
    <!-- Animate.css -->
    <link rel="stylesheet" href="{{ asset('css/helper/animate.css')}}">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="{{ asset('css/helper/icomoon.css')}}">
    <!-- Simple Line Icons -->
    <link rel="stylesheet" href="{{ asset('css/helper/simple-line-icons.css')}}">
    <!-- Owl Carousel  -->
    <link rel="stylesheet" href="{{ asset('css/helper/owl.carousel.min.css')}}">
    <!-- Custom CSS here-->
    <link rel="stylesheet" href="{{ asset('css/helper/owl.theme.default.min.css')}}">
    <!-- Style -->
    <link rel="stylesheet" href="{{ asset('css/helper/style.css')}}">


    <link href="{{ asset('css/app.css') }}" rel="stylesheet">  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
    <link href="{{ asset('css/master.css') . '?v=1518846252668' }}" rel="stylesheet">
    
   
    <!-- Modernizr JS -->
    <script src="{{ asset('js/helper/modernizr-2.6.2.min.js')}}"></script>
    
    
</head>
<body>
    <div id="app">
        @include('inc.navbar')

        <div class="container-fluid">
            @yield('content')
        </div>
    </div>

    <!-- Scripts -->


  
    <script src="{{ asset('js/app.js') }}"></script> 
    {{--  <script src="{{ asset('js/helper/jquery.min.js')}}"></script>   --}}
    <script src="//code.jquery.com/jquery.js"></script> 
    <!-- jQuery -->
    
    <!-- jQuery Easing -->
    <script src="{{ asset('js/helper/jquery.easing.1.3.js')}}"></script>
     <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script> 
    <!-- Waypoints -->
    <script src="{{ asset('js/helper/jquery.waypoints.min.js')}}"></script>
    <!-- Stellar Parallax -->
    <script src="{{ asset('js/helper/jquery.stellar.min.js')}}"></script>
    <!-- Owl Carousel -->
    <script src="{{ asset('js/helper/owl.carousel.min.js')}}"></script>
    <!-- Counters -->
    <script src="{{ asset('js/helper/jquery.countTo.js')}}"></script>


    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>  
    <!-- Bootstrap -->
	{{--  <script src="{{ asset('js/helper/bootstrap.min.js')}}"></script>  --}}
    
    <!-- Custom scripts here-->
    <!-- Main JS (Do not remove) -->
    <script src="{{ asset('js/helper/main.js')}}"></script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
       
    </script>
    <script>
        @if(Session::has('message'))
            var type = "{{ Session::get('alert-type', 'info') }}";
            switch(type){
                case 'info':
                    toastr.info("{{ Session::get('message') }}");
                    break;

                case 'warning':
                    toastr.warning("{{ Session::get('message') }}");
                    break;

                case 'success':
                    toastr.success("{{ Session::get('message') }}");
                    break;

                case 'error':
                    toastr.error("{{ Session::get('message') }}");
                    break;
            }
        @endif
    </script>
    {!! Toastr::render() !!}
    @stack('scripts')
</body>
</html>
