<?php

namespace App\Http\Controllers;

use Exception;
use App\Agency;
use App\Flight;
use App\Traveller;
use App\ReferralSrc;
use Illuminate\Http\Request;

class FlightController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ref = ReferralSrc::all();
        $agency = Agency::first();
        return view('flight.add', [
            'ref' => $ref,
            'agency' => $agency
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $flight = new Flight();
            $flight->email = $request->input('email');
            $flight->phone = $request->input('phone');
            $flight->travel_from_date = $request->input('travel_from_date');
            $flight->travel_to_date = $request->input('travel_to_date');
            $flight->travel_from_loc = $request->input('travel_from_loc');
            $flight->travel_to_loc = $request->input('travel_to_loc');
            $flight->other_flight_details = $request->input('other_flight_details');
            $flight->visa_interview_date = $request->input('visa_interview_date');
            $flight->visa_consulate = $request->input('visa_consulate');
            $flight->referral_src_code = $request->input('referral_src_code');
            $flight->is_traveler = ($request->input('is_traveler') == 1 ? true : false);
            $flight->flight_price = $request->input('flight_price');
            $flight->total_payment = $request->input('total_payment');
            $flight->save();

            $fname = $request->get('fname');
            $lname = $request->get('lname');
            echo count($fname);
            for($i = 1; $i <= count($fname); $i++){
                $trav = new Traveller();
                $trav->flight_id = $flight->id;
                $trav->first_name = $fname[$i];
                $trav->last_name = $lname[$i];
                $trav->save();
            }

            $notification = array(
                'message' => 'Flight successfully added',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        } 
        catch(Exception $e){
            
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
